// this one will save the game at different turn's after saving game it will display that game has been saved..it means that game has been saved at that turn...
//  after saving game , game will be continue... and for exit we enter [E/e]..... and if user want to continue after saving game it will work...
# include <iostream>
# include <fstream>
# include <cstring>
# include <ctime>// for random funtion;
# include <conio.h>// for getch();
# include <stdlib.h>
# include <Windows.h>// FOR beep, clear screen
using namespace std;
class tic_tac_toe
{
	int size;// it is the size of two demension arry row =size and colume=size
	int **p; // it will allocate two demension array
	int colume;// it will use to contain colume number ..
	char date[45];
	int fs_se_player;
	int pointer_ass[45];// we will assign two demenstion pointer in this array and save it in file... size is 45 that is maximum for 5X5
	int com_or_hum;
public:
	int comp_or_humanf();
	int win_cheak; // it is public so that can be use in main, if game win or draw it became one and restrict users to enter more input
	int drawn; // it is use in ... COndition cheak funtion... on call cheak funtion it incrememt by 1 
	tic_tac_toe()	// by default object will allocate 3 row and 3 colume
	{
		this->size=3;
		this->p=new int*[3];
		for(int i=0;i<3;i++)
		{
			this->p[i]=new int [3];
		}
	}
	void memory(int size)// user selected size of the two demension array
	{
		this->size=size;
		this->p=new int*[size];
		for(int i=0;i<size;i++)
		{
			this->p[i]=new int [size];
		}

	}
	bool play_again();// if user want to play game after using saving game or after deleting game
	static void instruction();
	int toss();
	void show_time();// this function show time at which game save
	void delete_all(); // if user wants to delete all games
	void task(int);// in this funtion if the save game between 2 players then it will execute in pattren by pattren
	void save_game(int); // it will save game
	void load_game(); // it will load game
	void delt_game(); // this_one will delete the game
	void pervious_games();// this function ask about perious games..
	void computer();
	int first_computer();// it will cheak wheater any one place do win to computer
	void game_dealer();// it is well-come screen e.tc.
	int computer_line();// it is use to make a line... may be vertical horizontal or daignol
	int cheak_place();	// it will restrck user from win
	void player_one(); // player one will enter input in this function
	void start_pattren(); // it will show user how to play and at which place he or she can enter
	void player_two();// player two will enter input in ths function
	void after_input_pattren();
	void cheak(); // it will cheak wheter player win or not
	void win(int);// if any player win it will show msg
	void drawn_game();// if game draw it will show "Drawn msg and beep"
	~tic_tac_toe()	// it will deallocate the memory
	{
		for(int i=0;i<size;i++)
		{
			delete [] p[i];
		}
		delete []p; 
	}
};
void tic_tac_toe::delete_all()
{
	fstream fio("Game.txt",ios::out | ios::binary);// for delete all date in file i will open it in out mode
	if(!fio)
	{
		cout<<"Empty Saved Game"<<endl;
	}
	fio.close();
	cout<<"Successfully Deleted"<<endl;
}
bool tic_tac_toe::play_again()
{
	bool val;
	char cntinue;
	Beep(1000,100);		// after win or draw program will beep
	cout<<"Want to Start Again [Y/y] :";		// after playing game it will ask wheter user wants to play a new game
	cin>>cntinue;
	if(cntinue=='y' || cntinue=='Y')// if user want to play again after using saving game then he/she will press yes otherwise game will b terminate
		val=true;
	else val=false;
		return val;
}
void tic_tac_toe::task(int a)// a tell about will user now take its turn
{
	if(this->com_or_hum==1)// if com_or_human variable is one it tells that save game was between in two players
	{
		do
		{
				if(this->win_cheak==0 && a!=2)// it will be true until game drawn or win, if a is 2 it means that player two will put the value
				{
				this->player_one();
				}
				if(this->win_cheak==0)
				{
				this->player_two();
				}
				a=0;// if a=0 then pattren will take place so now there is no concern from 2
		}while(this->win_cheak!=1);
	}
	if(this->com_or_hum==2)
	{
		do
		{
				if(this->win_cheak==0 && a==2)// it will be true until game drawn or win, if a==2 computer will select the place
				{
					this->computer();
				}
				if(this->win_cheak==0)
				{
					this->player_one();
				}
				a=2;
		}while(this->win_cheak!=1);
	}
	Sleep(3000);
	system("cls");
}
void tic_tac_toe::load_game()
{
	char state[20];
	int place;
	int move;
	fstream fio("Game.txt",ios::in|ios::binary);
	if(!fio) 
	{
		cout<<"File Not Found"<<endl;
	}
	fio.seekg(0,ios::end);
	int size=fio.tellg()/(sizeof(*this));
	if(size==0)// if size==0 its means that file is empty 
	{
		cout<<"Empty Saved Game"<<endl;
	}
	else
	{
		do
		{
			cout<<"Which State You Want To Load :";// after showing time i will ask from user that which saved game he/she wants to play
			cin>>state;
			place=atoi(state);
		}while(place<=0 || place>size);
		cout<<"Waiting . . ."<<endl;
		Sleep(1500);
		fio.seekg(0,ios::beg);
		place=place-1;
		move=place*(sizeof(*this));
		fio.seekg(move,ios::beg);
		fio.read(reinterpret_cast<char*>(this),sizeof(*this));//this one will read all the write object and then due to dis we can get the perivious data
		this->memory(this->size);// after getting size it will allocate memory for a pointer
		for(int i=0,k=0;i<this->size;i++)
		{
			for(int j=0;j<this->size;j++,k++)
			{
				this->p[i][j]=pointer_ass[k];// it will assign the last user or computer selected place in pointer
				cout<<this->p[i][j]<<endl;
			}
		}
		if(this->fs_se_player==1)
		{
			system("cls");
			cout<<"Load Successfully"<<endl;
			this->after_input_pattren();
			this->player_one();
			this->task(2);
		}
		else if(this->fs_se_player==2)
		{
			system("cls");
			this->after_input_pattren();
			this->player_two();
			this->task(0);
		}
		fio.close();
	}

}
void tic_tac_toe::delt_game()
{
	char state[20];
	int place;
	int count=1;
	fstream fio("Game.txt",ios::in|ios::binary);
	if(!fio) 
	{
		cout<<"File Not Found"<<endl;
	}
	fio.seekg(0,ios::end);
	int size=fio.tellg()/(sizeof(*this));
	if(size==0)
	{
		cout<<"Empty Saved Gaem"<<endl;
	}
	else
	{
		fstream fout("New.txt",ios::out | ios::binary);
		do
		{
			cout<<"Which State You Want To Delete :";//after showing time i will ask that which game user wants to delete
			cin>>state;
			place=atoi(state);
		}while(place<=0 || place>size);
		cout<<"Waiting . . ."<<endl;
		Sleep(1500);
		fio.seekg(0,ios::beg);
		while(count<=size)// if the user selected place found we will not write the object of this game in other file(new.txt)
		{
			fio.read(reinterpret_cast<char*>(this),sizeof(*this));
			if(place!=count)// if user selected game or read games arre not equal then this one will write the object in other file(new.txt)
			{
				fout.write(reinterpret_cast<char*>(this),sizeof(*this));
			}
			count++;
		}
	fio.close();
	fout.close();
	remove("Game.txt");
	rename("New.txt","Game.txt");
	cout<<"Delete SUccessfully"<<endl;
	}
	
}
void tic_tac_toe::show_time()
{
	fstream fio("Game.txt",ios::in|ios::binary);
	if(!fio)
	{
		cout<<"File Not Found"<<endl;
	}
	fio.seekg(0,ios::end);
	int size=fio.tellg()/(sizeof(*this));
	int count=0;
	fio.seekg(0,ios::beg);
	while(count<size)
	{
		fio.read(reinterpret_cast<char*>(this),sizeof(*this));// this one read squential data in object ... 
		cout<<count+1<<".\tGame Saved at "<<this->date;//after reading in object we cout the time 
		count++;
	}
	fio.close();
}
void tic_tac_toe::save_game(int player)// this function will saved the game, player will tell the next turn will player do...
{
	time_t now=time(0);
	char *date=ctime(&now);
	strcpy(this->date,date);
	this->fs_se_player=player;
	for(int i=0,k=0;i<this->size;i++)
	{
		for(int j=0;j<this->size;j++,k++)
		{
			this->pointer_ass[k]=p[i][j];
		}
	}
	fstream fio("Game.txt",ios::out | ios::app| ios::binary);// after opening file in append mode we will be able to write the next data in the file
	fio.seekp(0,ios::end);
	if(!fio)
	{
		cout<<"Empty Saved Game"<<endl;
	}
	fio.write(reinterpret_cast<char*>(this),sizeof(*this));
	fio.close();
}
void tic_tac_toe::pervious_games()
{
	char choice[20];
	int count;
	int convert;
	bool val2;
	bool val=true;
	do
	{
		count=0;
		cout<<"\t\t***************************"<<endl;
		cout<<"\t\t  1:Load A Saved Game"<<endl;
		cout<<"\t\t  2:Delete Saved Game"<<endl;
		cout<<"\t\t  3:New Game"<<endl;
		cout<<"\t\t  4:Delete All Saved Games"<<endl;
		cout<<"\t\t***************************"<<endl;
		cout<<"Enter Choice :";cin>>choice;
		for(int i=0;choice[i]!=0;i++)
		{
			count++;
		}
		if(count==1)
		{
			convert=atoi(choice);
			if(convert==0 || convert >4)
			{
				val=true;
			}
			else val=false;
		}
		system("cls");
	}while(val);
	if(convert==1)
	{
		this->show_time();// this one will show the time so tha we can know that about games...
		this->load_game();// after showing time we will tell in this function that which game we want to load
		val2=this->play_again();// this funtion will ask that user want to play a game or not.... is yes it will return true otherwise fals
		if(val2)// if ture then game will be continue from starting
		{
			system("cls");
			this->game_dealer();
			cout<<"\t\t\t******************************"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\tThanks For Playing Tic-Tak-Toe"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\t******************************"<<endl<<endl;
			exit(0);
		}
		else// if val2=false then game will be terminate
		{
			cout<<"\t\t\t******************************"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\tThanks For Playing Tic-Tak-Toe"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\t******************************"<<endl<<endl;
			exit(0);
		}
	}
	else if(convert==2)
	{
		this->show_time();
		this->delt_game();
		val2=this->play_again();
		system("cls");
		if(val2)
		{
			system("cls");
			this->game_dealer();
			cout<<"\t\t\t******************************"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\tThanks For Playing Tic-Tak-Toe"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\t******************************"<<endl<<endl;
			exit(0);
		}
		else
		{
			cout<<"\t\t\t******************************"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\tThanks For Playing Tic-Tak-Toe"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\t******************************"<<endl<<endl;
			exit(0);
		}
	}
	else if(convert==4)
	{
		this->delete_all();
		val2=this->play_again();
		system("cls");
		if(val2)
		{
			system("cls");
			this->game_dealer();
		}
		else
		{
			cout<<"\t\t\t******************************"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\tThanks For Playing Tic-Tak-Toe"<<endl;
			cout<<"\t\t\t                              "<<endl;
			cout<<"\t\t\t******************************"<<endl<<endl;
			exit(0);
		}
	}
}
int tic_tac_toe::computer_line()// this is additional without this computer will give hard time to user.... but this funtion will give more tuff time... and may be user would not win.... 
{
	int check;	// it will check how many that '1' time if condition execute or not
	int count;// it will count that place where input of 2nd user will be not....
	int ass2;
	for(int i=0;i<this->size;i++)
	{
		check=0;
		ass2=0;
		this->colume=0;
		count=0;
		for(int j=0; j<this->size;j++)
		{
			if(p[i][j]!=size*size+1 || p[i][j]==size*size+3)// first check that place contain 'B' and not "A' A is the input of the 2nd user
			{
				if(p[i][j]!=size*size+3)		// it will check that place contain 'B' or any digit
				{
					this->colume=j;
					ass2=i;
					check=1;
				}
				count++;
			}
		}
		if(count==this->size && check==1)
		{
			return ass2;
		}
		
	}
	for(int i=0;i<this->size;i++) 
	{
		check=0;
		this->colume=0;
		ass2=0;
		count=0;
		for(int j=0; j<this->size;j++)
		{
			if(p[j][i]!=size*size+1 || p[j][i]==size*size+3)
			{
				if(p[j][i]!=size*size+3)
				{
					this->colume=i;
					ass2=j;
					check=1;
				}
				count++;
			}
		}
		if(count==this->size && check==1)
		{
			return ass2;
		}
		
	}
	ass2=0;
	check=0;
	this->colume=0;
	count=0;
	for(int i=0;i<this->size;i++)
	{
		if(p[i][i]!=size*size+1 || p[i][i]==size*size+3)
		{
			if(p[i][i]!=size*size+3)
			{
				this->colume=i;
				check=1;
			}
			count++;
		}
		if(count==this->size && i==size-1 && check==1)
		{
			return this->colume;
		}
	}
	check=0;
	this->colume=0;
	count=0;
	int ass;
	for(int i=this->size-1,j=0;i>=0;i--,j++) // it will check secondry diagonal
	{
		if(p[j][i]!=size*size+1 || p[j][i]==size*size+3)
		{
			if(p[i][j]!=size*size+3)
			{
				this->colume=i;
				ass=j;
				check=1;
			}
			count++;
		}
		if(count==this->size && j==size-1 && check==1)
		{
			return ass;
		}
	}
	return -3;
}
int tic_tac_toe::first_computer() // it will check any place do win to computer
{
	int count;
	int ass2;
	for(int i=0;i<this->size;i++)
	{
		this->colume=0;
		count=1;
		for(int j=0; j<this->size;j++)
		{
			if(p[i][j]==size*size+1 || p[i][j]==size*size+3)	// it will use to find empty place where computer put
			{
				if(p[i][j]==size*size+1)
				{
					count=count+size;	// if i did not use this cheak it will count to 2nd player  place also
				}
				count++;
			}
			else
			{
				this->colume=j;// it colume variable , it will place the colume number of empty place where computer can put
			}
		}
		if(count==this->size)
		{
			return i;// it will return row... by using "row" and this->colume is function ... computer will be able to put " B' so that it can make its line
			
		}
		
	}
	this->colume=0;
	count=1;
	for(int i=0;i<this->size;i++) 
	{
		count=1;
		for(int j=0; j<this->size;j++)
		{
			if(p[j][i]==size*size+1 || p[j][i]==size*size+3)	// it will use to find empty place where computer put
			{
				if(p[j][i]==size*size+1)
				{
					count=count+size;
				}
				count++;
			}
			else
			{
				this->colume=i;
				ass2=j;// it will return row... by using "row" and this->colume is function ... computer will be able to put " B' so that it can make its line
			}
		}
		if(count==this->size)
		{
			return ass2;
		}
		
	}
	this->colume=0;
	count=1;
	for(int i=0;i<this->size;i++)
	{
		if(p[i][i]==size*size+1 || p[i][i]==size*size+3)// it will use to find empty place where computer put
		{
			if(p[i][i]==size*size+1)
			{
				count=count+size;
			}
			count++;
		}
		else 
		{
			this->colume=i;
		}
		if(count==this->size && i==size-1)
		{
			return this->colume;// it will return row... by using "row" and this->colume is function ... computer will be able to put " B' so that it can make its line
		}
	}
	this->colume=0;
	count=1;
	int ass;
	for(int i=this->size-1,j=0;i>=0;i--,j++) // it will check secondry diagonal
	{
		if(p[j][i]==size*size+1 || p[j][i]==size*size+3)
		{
			if(p[i][j]==size*size+1)
			{
				count=count+size;
			}
			count++;
		}
		else
		{
			this->colume=i;
			ass=j;
		}
		if(count==this->size && j==size-1)
		{
			return ass;
		}
	}
	return -2;
}
int tic_tac_toe::cheak_place()
{
	int count;
	int ass2;
	for(int i=0;i<this->size;i++)
	{
		this->colume=0;
		count=1;
		for(int j=0; j<this->size;j++)
		{
			if(p[i][j]==size*size+1 || p[i][j]==size*size+3)// if will check how to restric user , in it we count the place where 2nd player have put if count==size it means one place left for user to win now computer will put its input there
			{
				if(p[i][j]==size*size+3)
				{
					count=count+size;
				}
				count++;
			}
			else
			{
				this->colume=j;
			}
		}
		if(count==this->size)
		{
			return i;// rows
			
		}
		
	}
	this->colume=0;
	count=1;
	for(int i=0;i<this->size;i++) 
	{
		count=1;
		for(int j=0; j<this->size;j++)
		{
			if(p[j][i]==size*size+1 || p[j][i]==size*size+3)// if will check how to restric user , in it we count the place where 2nd player have put if count==size it means one place left for user to win now computer will put its input there
			{
				if(p[j][i]==size*size+3)
				{
					count=count+size;
				}
				count++;
			}
			else
			{
				this->colume=i;
				ass2=j;
			}
		}
		if(count==this->size)
		{
			return ass2;
		}
		
	}
	this->colume=0;
	count=1;
	for(int i=0;i<this->size;i++)
	{
		if(p[i][i]==size*size+1 || p[i][i]==size*size+3)// if will check how to restric user , in it we count the place where 2nd player have put if count==size it means one place left for user to win now computer will put its input there
		{
			if(p[i][i]==size*size+3)
			{
				count=count+size;
			}
			count++;
		}
		else 
		{
			this->colume=i;
		}
		if(count==this->size && i==size-1)
		{
			return this->colume;
		}
	}
	this->colume=0;
	count=1;
	int ass1;
	for(int i=this->size-1,j=0;i>=0;i--,j++) // it will check secondry diagonal
	{
		if(p[j][i]==size*size+1 || p[j][i]==size*size+3)// if will check how to restric user , in it we count the place where 2nd player have put if count==size it means one place left for user to win now computer will put its input there
		{
			if(p[j][i]==size*size+3)
			{
				count=count+size;
			}
			count++;
		}
		else
		{
			this->colume=i;// this will return colume number of place
			ass1=j;// this will return row ... now by the combination of row and colume that place will fount where computer can put....because of this user will not be able to win
		}
		if(count==this->size && j==size-1)
		{
			return ass1;
		}
	}
	return -1;
}
void tic_tac_toe::computer()
{
	Sleep(1200);
	int t=time(0);
	srand(t);
	bool val=true;
	int ran;
	int ret_val2=this->first_computer();
	if(ret_val2!=-2)// if computer has already tick to 2 consective place then it will put on the 3rd place... int 3 X3 .... and if there in 3 consective in 4 x4 it will place at the 4th place
	{
		p[ret_val2][this->colume]=size*size+3;
	}
	else
	{
		ret_val2=this->cheak_place();// it will restric user
		if(ret_val2!=-1)
		{
			p[ret_val2][this->colume]=size*size+3;
		}
		else
		{
			ret_val2=this->computer_line();
			if(ret_val2!=-3)		// it will make its line
			{
				p[ret_val2][this->colume]=size*size+3;
			}
			else
			{
		
				do
				{ 
					ran=1+rand()%(size*size);
					for(int i=0;i<size;i++)
					{
						for(int j=0;j<size;j++)
						{
							if(p[i][j]==ran && ran>=0 && ran<=size*size)		// cheak weather input is equat to daynamically array data if place one is 1 and user will enter 1 then at place one B will b placed
							{
								p[i][j]=size*size+3;
								val=false;

							}

						}
					}
				}while(val);
			}
		}
	}
	system("cls");
	this->after_input_pattren();
	this->cheak();


}
int tic_tac_toe::comp_or_humanf()
{
	char com_hum_choice[20];
	bool val2;
	int ret;
		do
			{
				cout<<"1: Multi. Player( Human Vs Human )"<<endl;
				cout<<"2: Single Player( Human Vs Computer )"<<endl<<endl;
				cout<<"Enter Your Choice:";
				cin>>com_hum_choice;
				com_hum_choice[0]=com_hum_choice[0]-48;
				if(com_hum_choice[0]=='e' || com_hum_choice[0]=='E')
				{
					exit(0);
				}
				if(com_hum_choice[0] == 1 || com_hum_choice[0]==2)
				{
					val2=false;
					ret=com_hum_choice[0];
				}
				else val2=true;

			}while(val2);
	return ret;
}
int tic_tac_toe::toss()
{
	bool val=true;
	cout<<"\t\t***********************************"<<endl;
	cout<<"\t\t*                                 *"<<endl;
	cout<<"\t\t*              TOSS               *"<<endl;
	cout<<"\t\t*                                 *"<<endl;
	cout<<"\t\t***********************************"<<endl;
	unsigned int t=time(0);
	srand(t);
	char user_choice[20];
	int con_choice;		// in it we assign the char to int value...
	cout<<endl<<"1 . Head"<<endl;
	cout<<"2 . Tail"<<endl;
	do
	{
		cout<<endl<<"Enter Choice For Toss:";cin>>user_choice;
		con_choice=atoi(user_choice);		// if user enter alphabets then it will not crash....  cheak first digit if it is one or two then while terminate for example if user enter 1afde it will only take one, if user enter abcd then in int variable 0 assign
		if(con_choice == 1 || con_choice==2)
		{
			val=false;
		}
	}while(val);
	int a;
	a=rand()%2+1;
	if(a==con_choice)
	{
		system("cls");
		this->start_pattren();
		cout<<"Win ! You Are the 1ST Player"<<endl;
		Beep(10000,1000);
		return 1;
	}
	else
	{
		system("cls");
		this->start_pattren();
		cout<<"Lose ! You Are the 2ND Player"<<endl;
		Beep(1000,1000);
		return 2;
	}

}
void tic_tac_toe::instruction() // Instruction to user how to play a game
{
	cout<<endl<<"INSTRUCTIONS ......."<<endl<<endl;
	cout<<"[ Just Select Your Place By Enter Digit ]"<<endl;
	cout<<"[ For First Player System will put A Atomatically ]"<<endl;
	cout<<"[ For Second Player System will put B Atomatically ]"<<endl;
	cout<<"[ Keep Caps Lock Off,Otherwise You Will not be Able to Select the Right Place !!! ]"<<endl<<endl;
	cout<<"Press any key . . .";_getch(); // put any key after reading instructionss...
	system("cls");
};
void tic_tac_toe::drawn_game()
{
	cout<<endl<<"Game Draw !!!"<<endl;
	this->win_cheak=1;// it will restrict users to enter input
}
void tic_tac_toe::win(int a)
{
	if(a-(size*size)==3)
		cout<<endl<<"Computer Win"<<endl;
	if(a-(size*size)==1 || a-(size*size)==2)
	cout<<endl<<"Player "<<a-size*size<<" Wins !!!"<<endl;// the value will be in char so for display one or two we subract 48...
	this->win_cheak=1;
}
void tic_tac_toe::cheak() // this check will work generically... if user want to play n X n then it will also work
{
	this->drawn++;// it will increment on every call of check and at the end decide wheter game draw or not
	int count=1;
	int ass;// it assign the the first value of row or colume or daglone
	bool val=false;// if "val" bacame true in any loop then other loops will not work..
	for(int i=0;i<this->size && val==false;i++) // this loop cheak all horizontal value... if and row have same value , val became true and other condition will not work...
	{
		count=1;
		for(int j=0; j<this->size;j++)
		{
			if(j==0)
			{
				ass=this->p[i][j];
			}
			else if(ass==this->p[i][j])
			{
				count++;
			}
		}
		if(count==this->size)// if in a row all value equal then it will be equal to the size of row  so val became true and other loops will not work
		{
			val=true;
			i=size;// it will work like break....
			
		}
		
	}
	count=1;
	for(int i=0;i<this->size && val==false;i++) // it will cheak all colume , if any colume have same value , "val became true" and other loops will not work
	{
		count=1;
		for(int j=0; j<this->size;j++)
		{
			if(j==0)
			{
				ass=this->p[j][i];
			}
			else if(ass==this->p[j][i])
			{
				count++;
			}
		}
		if(count==this->size)
		{
			val=true;
			i=size;
		}
		
	}
	count=1;
	for(int i=0;i<this->size && val==false;i++) // it will  check principle diagonal
	{
		if(i==0)
			ass=p[i][i];
		else if(ass==p[i][i])
		{
			count++;
		}
		if(count==this->size)
		{
			val=true;
		}
	}
	count=1;
	for(int i=this->size-1,j=0;i>=0 && val==false;i--,j++) // it will check secondry diagonal
	{
		if(j==0)
			ass=p[j][i];
		else if(ass==p[j][i])
		{
			count++;
		}
		if(count==this->size)
		{
			val=true;
		}
	}
	if (val) // if true it means in any loop row elements,colume element or diagonal elements are same
	{
		this->win(ass);
	}
	else if(this->drawn==this->size*this->size)// if all place are reserved but no result found then it will excute
	{
		this->drawn_game();
	}
}
void tic_tac_toe::after_input_pattren()// it will display the pattern after enter input by user 1 or 2 , and in other word show at which place user enter
{
	cout<<"For Save Press [S/s] And For Exit [E/e] :"<<endl<<endl;
	for(int i=0;i<this->size;i++)
	{
		for(int i=0;i<5*size;i++)
		{
			if(i==0)
			cout<<"\t\t\t_";
			cout<<"_";
		}
		cout<<endl;
		for(int j=0;j<this->size;j++)
		{
			if(j==0)
			{
				cout<<"\t\t\t|";
			}
			if(p[i][j]==size*size+1)
			{
				cout<<"A"<<"   |";
			}
			else if((p[i][j])==size*size+2 || (p[i][j])==size*size+3)
			{
				cout<<"B"<<"   |";
			}
			if(p[i][j]<10 && p[i][j]!=size*size+1 && p[i][j]!=size*size+2 && p[i][j]!=size*size+3 )
			{
			cout<<p[i][j]<<"   |";
			}
			if(p[i][j]<100 && p[i][j]>=10 && p[i][j]!=size*size+1 && p[i][j]!=size*size+2 && p[i][j]!=size*size+3)
			{
				cout<<p[i][j]<<"  |";
			}
				
		}
		cout<<endl;
		for(int i=0;i<size;i++)
		{
			if(i==0)
			cout<<"\t\t\t|  ";
			cout<<"  |  ";
		}
		cout<<endl;
	}
		for(int i=0;i<5*size;i++)
		{
			if(i==0)
			cout<<"\t\t\t_";
			cout<<"_";
		}
		cout<<endl;
}
void tic_tac_toe::player_two()
{
	bool val=true;
	int count_alph;
	int convert=0;
	int after_saving=0;
	char Player_two[20];
	int i=GetKeyState(VK_CAPITAL);// it will cheak wheater capslock on or of if on it will give 1 otherwise 0
	if(i==1)
	{
		Beep(1000,1000);
		cout<<endl<<"Please OFF CapsLock !!!"<<endl<<endl;
	}
	do
	{
		count_alph=0;
		cout<<"Player 2 Select a location :";
		cin>>Player_two;
		for(int i=0;Player_two[i]!=0;i++)
		{
			count_alph++;
		}
		convert=atoi(Player_two);// if user enter alphabet it will be wrong input if user enter number greater than place such as 23dddds it take 23 as 23 place not available in 4 x 4 it will be wrong input 
		if(count_alph==1)// this condition check that wheter two char in player two, if yes then true
		{
			if(Player_two[0]=='s' || Player_two[0]=='S')// if user enter first charcter s game will be save... game will not terminta
			{
				this->save_game(2);
				after_saving=1;
				cout<<"Game Has Been Saved"<<endl;
				cout<<"Press [E/e] For Exit"<<endl;
				//exit(0);
			}
			else if(Player_two[0]=='e' || Player_two[0]=='E')
			{
				cout<<"\t\t\t******************************"<<endl;
				cout<<"\t\t\t                              "<<endl;
				cout<<"\t\t\tThanks For Playing Tic-Tak-Toe"<<endl;
				cout<<"\t\t\t                              "<<endl;
				cout<<"\t\t\t******************************"<<endl<<endl;
				exit(0);
			}
		}
		for(int i=0;i<size;i++)
		{
			for(int j=0;j<size;j++)
			{
				if(p[i][j]==convert && convert>=0 && convert<=size*size)		// cheak weather input is equat to daynamically array data if place one is 1 and user will enter 1 then at place one B will b placed
				{
					p[i][j]=size*size+2;
					val=false;

				}

			}
		}
		if(val && after_saving!=1)
		{
			cout<<"Invalid Input OR Caps Lock On"<<endl;
		}
		after_saving=0;
	}while(val);
	system("cls");
	this->after_input_pattren();
	this->cheak();
}
void tic_tac_toe::player_one()
{
	char Player_one[20];
	int convert=0;
	int after_save=0;
	int count_alph; // it will check how many character user enter in its input if it is more than 1 , it will be invalid input
	bool val=true;
	int i=GetKeyState(VK_CAPITAL);// it will cheak wheater capslock on or of if on it will give 1 otherwise 0
	if(i==1)
	{
		Beep(1000,1000); // when capslock program will beep
		cout<<endl<<"Please Off CapsLock"<<endl<<endl;
	}
	do // this loop will not work when user enter a valid input...
	{
		count_alph=0;
		cout<<"Player 1 Select a location :";
		cin>>Player_one;
		for(int i=0;Player_one[i]!=0;i++)
		{
			count_alph++;
		}
		convert=atoi(Player_one);
		if(count_alph==1)
		{
			if(Player_one[0]=='s' || Player_one[0]=='S')// after saving game will not terminate... it will save the game at the point....and game will be continue
			{
				this->save_game(1);
				cout<<"Game Has Been Saved"<<endl;
				cout<<"Press [E/e] For Exit"<<endl;
				after_save=1;
				//exit(0);
			}
			if(Player_one[0]=='e' || Player_one[0]=='E')
			{
				cout<<"\t\t\t******************************"<<endl;
				cout<<"\t\t\t                              "<<endl;
				cout<<"\t\t\tThanks For Playing Tic-Tak-Toe"<<endl;
				cout<<"\t\t\t                              "<<endl;
				cout<<"\t\t\t******************************"<<endl<<endl;
				exit(0);
			}
		}
		for(int i=0;i<size;i++)
		{
			for(int j=0;j<size;j++)
			{
				if(p[i][j]==convert && convert>=0 && convert <=size*size)
				{
					p[i][j]=size*size+1;
					val=false;

				}

			}
		}
		if(val && after_save!=1)
		{
			cout<<"Invalid Input OR Caps Lock On"<<endl;
		}
		after_save=0;
	}while(val);
	system("cls");
	this->after_input_pattren();
	this->cheak();// it will cheak wheter game end or not
}
void tic_tac_toe::start_pattren() // IT will show to user how game will seem him/her and how he can enter ur choice at different places
{
	system("cls");
	cout<<"For Save Press [S/s] And For Exit [E/e] :"<<endl<<endl;
	int a=1;	// in 2'd first value will be 1 and then it increment by '1'
	for(int i=0;i<this->size;i++)
	{
		for(int j=0;j<this->size;j++)		// this loop will store the digit in 2'd array
		{
			p[i][j]=a++;
		}
	}
	for(int i=0;i<this->size;i++)
	{
		for(int i=0;i<5*size;i++)
		{
			if(i==0)
			cout<<"\t\t\t_";
			cout<<"_";				// it will print line such as " _______________________________"
		}
		cout<<endl;
		for(int j=0;j<this->size;j++)
		{
			if(j==0)
			{
				cout<<"\t\t\t|";
			}
			if(p[i][j]<10)	
			{
				cout<<p[i][j]<<"   |";
			}
			if(p[i][j]<100 && p[i][j]>=10)
			{
				cout<<p[i][j]<<"  |";
			}
		}
		cout<<endl;
		for(int i=0;i<size;i++)
		{
			if(i==0)
			cout<<"\t\t\t|  ";
			cout<<"  |  ";
		}
		cout<<endl;
	}
		for(int i=0;i<5*size;i++)
		{
			if(i==0)
			cout<<"\t\t\t_";
			cout<<"_";
		}
		cout<<endl;
		
		
}
void tic_tac_toe::game_dealer()
{
	char cntinue;
	char selected_grid[20];		// it will store the value of user selection 
	bool val;
	
	int again; //  if user give value less than 3 or greater than 5 it will become '1' and display the msg that user input wrong number
	do
	{
			this->drawn=0;
			again=0;
			val=true;
			this->win_cheak=0;
			cout<<"\t\t\tWelcome to Tic-Tac-Toe"<<endl<<endl;
			this->instruction();// after user selection, it will give insturction to user how to play a game
			cout<<endl;
			this->pervious_games();
			this->com_or_hum=this->comp_or_humanf();
			cout<<"\t\t    Select Playing Grid\n";
			cout<<" \t\t\t3. 3 x 3"<<endl;
			cout<<" \t\t\t4. 4 x 4"<<endl;
			cout<<" \t\t\t5. 5 x 5"<<endl;
			do
			{
				if(again==1)							// do while will cheak wheter the user input in between 3,4,5 or not if it is greater than 5 or less than 3 program promoter to user to enter agian
				{
					cout<<"Enter Your Choice Again(3,4,5) :";
					cin>>selected_grid;
					selected_grid[0]=selected_grid[0]-48;
				}
				else
				{
				cout<<"Enter Your Choice :";
				cin>>selected_grid;
				selected_grid[0]=selected_grid[0]-48;
				}
				if(selected_grid[0]>=3 && selected_grid[0]<=5)// it will check 3 x 3 , 4x4 or 5x5 
				{
					this->memory(selected_grid[0]);// memory funtion allocate the memory according to the user input
					this->start_pattren();
					val=false;
				}
				again=1;
			}while(val);
			int toss_dec=this->toss();
			
			if(this->com_or_hum==1)
			{
				do
				{
						if(this->win_cheak==0)// it will be true until game drawn or win
						{
						this->player_one();
						}
						if(this->win_cheak==0)
						{
						this->player_two();
						}
				}while(this->win_cheak!=1);
			}
			if(this->com_or_hum==2)
			{
				do
				{
						if(this->win_cheak==0 && toss_dec==2)// it will be true until game drawn or win
						{
							this->computer();
						}
						if(this->win_cheak==0)
						{
							this->player_one();
						}
						toss_dec=2;
				}while(this->win_cheak!=1);
			}
			Beep(1000,1000);		// after win or draw program will beep
			cout<<"Want to Play again [Y/y] :";		// after playing game it will ask wheter user wants to play a new game
			cin>>cntinue;
			system("cls");
	}while(cntinue=='y' || cntinue=='Y');
}
int main()
{
	tic_tac_toe obj;
	obj.game_dealer();
	cout<<"\t\t\t******************************"<<endl;
	cout<<"\t\t\t                              "<<endl;
	cout<<"\t\t\tThanks For Playing Tic-Tak-Toe"<<endl;
	cout<<"\t\t\t                              "<<endl;
	cout<<"\t\t\t******************************"<<endl<<endl;
}
