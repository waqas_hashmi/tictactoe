# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Tic-tac-toe is a paper-and-pencil game for two players, X and O, who take turns marking the spaces in a 3�3, 4 x 4 and 5x5 grid. The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row wins the game.

Features:

*	Single Player Feature ( You vs Computer	Multiple Player)

*	Code is Generic means you can play 3x3 , 4x4,5x5 and so on� 

*	Computer Intelligence 

*	Save Game and Delete Game

*	Modular code you can change it easily



### How do I get set up? ###
Usage:

1.	First read the instruction and then press any key for continue.

2.	Menu will appear, ask you for save game , new game and delete game. Press 3 for New game.

3.	Menu will appear, ask you multiplayer or single player. Enter you choice.

4.	Menu will appear, ask you which type of Grid you want to play. Enter you choice.

5.	Play your game. If you want to save game press S/s or for exit press E/e.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
Blog: http://programminglearner143.blogspot.com/2017/09/tic-tac-toe-introduction-tic-tac-toe.html

Email: waqashaxhmi143@gmail.com